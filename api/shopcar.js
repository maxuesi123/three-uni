import HttpRequest from '@/utils/httprequest.js'
//购物车页面的数据
const prodTagList=(params)=>HttpRequest.post( '/p/shopCart/info',params)
const indexImgs=(params)=>HttpRequest.get('/indexImgs',params)

module.exports={
  prodTagList,indexImgs
}