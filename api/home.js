import {request} from '../utils/httprequest.js'
// 轮播图接口
export const getIndexImgs = ({url}) => request({
	url,
	method:"GET"
});
// 通知文字轮播接口
export const getTopNoticeList = ({url}) => request({
	url,
	method:"GET"
});
// 每日上新标题  : {id: 1, title: "每日上新", seq: "3", style: "2"}
export const getProdTagList = ({url}) => request({
	url,
	method:"GET"
});
// 每日上新 商城热卖 更多宝贝标题 /prod/tag/prodTagList

