const brrUrl = 'https://bjwz.bwie.com/mall4j'

class HttpRequest{
	request (
		url,
		data,
		method,
		
	){
		uni.showLoading({
			title: '正在加载中...',
		})
		return new Promise((resolve, reject) => {
			uni.request({
				url: brrUrl + url,
				data,
				method,
				success: (res) => {
					
					if (res.statusCode == 200) {
						resolve(res);
						uni.hideLoading()
					} else if (res.statusCode === 401) {
						uni.showToast({
							title: '没有访问权限',
						})
					}
				},
				fail: (err) => {
					uni.hideLoading()
					reject(err);
					uni.showToast({
						title: '请求失败',
					})
				},
				complete() {
					uni.hideLoading()
				}
			})
		})
	}
	get(url,params={}){
		
		return this.request(url,'GET',params)
	}
	post(url,params){
	 	return this.request(url,'POST',params)
	}
}

export default new HttpRequest()
 // header: {
	// 	    //返回数据类型
	// 	    "content-type":" application/json; charset=utf-8",
	// 	    //设置用户访问的token信息
	// 	    "Authorization": '请求头信息',
	// 	  },
	
	
	